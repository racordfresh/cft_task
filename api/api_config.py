BASE_PATH = '/cft/test'
GET_BEARER_TOKEN_PATH = f'{BASE_PATH}/bearer'
GET_SALARY_PATH = f'{BASE_PATH}/salary'
GET_UPGRADE_PATH = f'{BASE_PATH}/upgrade'

GET_BEARER_TOKEN_SUMMARY = 'Возвращает auth token по логину и паролю'
GET_SALARY_SUMMARY = 'Возвращает информацию о заработной плате обратившегося сотрудника'
GET_UPGRADE_SUMMARY = 'Возвращает информацию о следующем повышении сотрудника'

###BEARER CONFIG
SECRET_KEY = "09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 30
