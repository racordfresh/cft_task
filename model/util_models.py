import datetime
from typing import Union

from pydantic import BaseModel


class TokenData(BaseModel):
    username: Union[str, None] = None


class User(BaseModel):
    username: str
    email: Union[str, None] = None
    full_name: Union[str, None] = None
    disabled: Union[bool, None] = None
    salary: Union[str, None] = None
    promotion_date: Union[datetime.datetime, None] = None


class UserInDB(User):
    hashed_password: str
