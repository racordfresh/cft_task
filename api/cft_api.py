from datetime import timedelta

from fastapi import Depends, FastAPI, HTTPException, status
from fastapi.security import OAuth2PasswordRequestForm
from typing_extensions import Annotated

from api.api_config import ACCESS_TOKEN_EXPIRE_MINUTES
from db.fake_db import fake_users_db
from model.response_models import Token, SalaryPromotionDate
from model.util_models import User
from security.auth_methods import authenticate_user, create_access_token, get_current_active_user

app = FastAPI()


@app.post("/token", response_model=Token)
async def login_for_access_token(
        form_data: Annotated[OAuth2PasswordRequestForm, Depends()]
):
    user = authenticate_user(fake_users_db, form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"sub": user.username}, expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "bearer"}


@app.get("/users/me/salary", response_model=SalaryPromotionDate)
async def read_own_salary_and_promotion_date(
        current_user: Annotated[User, Depends(get_current_active_user)]
):
    return SalaryPromotionDate(salary=current_user.salary, promotion_date=current_user.promotion_date)
