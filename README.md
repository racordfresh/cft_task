## Запуск приложения

Запустить команду в корне приложения

```sh
python -m uvicorn api.cft_api:app --reload --port 8080 
```

Поднимется веб-сервер по адресу 127.0.0.1:8080

## Как использовать

### Получение токена

Чтобы получить Bearer token, нужно отправить post запрос к endpoint /token

```sh
curl -X 'POST' \
  'http://127.0.0.1:8080/token' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -d 'grant_type=&username=frixfr&password=secret&scope=&client_id=&client_secret='
```

В ответ мы получим наш токен аутентификации, который действует 30 минут.

```sh
{
  "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJmcml4ZnIiLCJleHAiOjE2ODY2NzY1NDV9.2_XekDofi9frJBPDiHSA2yQMJCP6UExyS0fLVqUYS5s",
  "token_type": "bearer"
}
```

### Получение информации о зарплате и повышении

Чтобы получить информацию о зарплате и повышении, нужно отправить get запрос по endpoint /users/me/salary. При этом мы должны добавить header аутентификации вида -H 'Authorization: Bearer <тут должен быть полученный токен>

```sh
curl -X 'GET' \
  'http://127.0.0.1:8080/users/me/salary' \
  -H 'accept: application/json' \
  -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJmcml4ZnIiLCJleHAiOjE2ODY2NzY1NDV9.2_XekDofi9frJBPDiHSA2yQMJCP6UExyS0fLVqUYS5s'
```

В ответе мы получим следующее

```sh
{
  "salary": "50000",
  "promotion_date": "2023-07-01T00:00:00"
}
```

## Примечание

К сожалению, связи с базой данных нет. Имеется только "фейковая" база данных в файле db/fake_db.py в виде словаря. В которой имеется 2 пользователя frixfr, johndoe с паролями secret.
