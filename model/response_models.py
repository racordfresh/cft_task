import datetime

from pydantic import BaseModel


class Token(BaseModel):
    access_token: str
    token_type: str


class SalaryPromotionDate(BaseModel):
    salary: str
    promotion_date: datetime.datetime