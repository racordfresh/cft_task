import datetime

fake_users_db = {
    "johndoe": {
        "username": "johndoe",
        "full_name": "John Doe",
        "email": "johndoe@example.com",
        "hashed_password": "$2b$12$EixZaYVK1fsbw1ZfbX3OXePaWxn96p36WQoeG6Lruj3vjPGga31lW",
        "disabled": False,
        "salary": "10000",
        "promotion_date": datetime.datetime(year=2025, month=12, day=11)
    },
    "frixfr": {
        "username": "frixfr",
        "full_name": "Evgeniy Pulin",
        "email": "frixfr@gmail.com",
        "hashed_password": "$2b$12$EixZaYVK1fsbw1ZfbX3OXePaWxn96p36WQoeG6Lruj3vjPGga31lW",
        "disabled": False,
        "salary": "50000",
        "promotion_date": datetime.datetime(year=2023, month=7, day=1)
    }
}
